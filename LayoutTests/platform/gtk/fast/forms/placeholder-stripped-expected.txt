layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x240
  RenderBlock {HTML} at (0,0) size 800x240
    RenderBody {BODY} at (8,16) size 784x216
      RenderBlock {P} at (0,0) size 784x38
        RenderText {#text} at (0,0) size 753x38
          text run at (0,0) width 753: "CR and LF in placeholder values should be stripped. The first input element and the first textarea element should have a"
          text run at (0,19) width 748: "placeholder \"first line second line\", and the second input element and the second textarea element should have nothing."
      RenderBlock (anonymous) at (0,54) size 784x162
        RenderTextControl {INPUT} at (2,2) size 194x25 [bgcolor=#FFFFFF] [border: (2px inset #000000)]
        RenderBR {BR} at (198,5) size 0x19
        RenderTextControl {INPUT} at (2,31) size 194x25 [bgcolor=#FFFFFF] [border: (2px inset #000000)]
        RenderBR {BR} at (198,34) size 0x19
        RenderBR {BR} at (206,91) size 0x19
        RenderText {#text} at (0,0) size 0x0
layer at (13,75) size 188x19
  RenderBlock {DIV} at (3,3) size 188x19
layer at (13,104) size 188x19
  RenderBlock {DIV} at (3,3) size 188x19
layer at (10,130) size 202x44 clip at (11,131) size 200x42
  RenderTextControl {TEXTAREA} at (2,60) size 202x44 [bgcolor=#FFFFFF] [border: (1px solid #000000)]
    RenderBlock {DIV} at (3,3) size 196x19
layer at (10,182) size 202x44 clip at (11,183) size 200x42
  RenderTextControl {TEXTAREA} at (2,112) size 202x44 [bgcolor=#FFFFFF] [border: (1px solid #000000)]
    RenderBlock {DIV} at (3,3) size 196x19
