layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x576
      RenderBlock (anonymous) at (0,0) size 784x44
        RenderText {#text} at (0,0) size 745x44
          text run at (0,0) width 745: "Test the changing of an input type=TEXT to type=IMAGE. This test is to make sure that the height and width"
          text run at (0,22) width 321: "attributes are used for the new type of IMAGE."
      RenderBlock {FORM} at (0,44) size 784x45
        RenderBR {BR} at (0,0) size 0x22
        RenderText {#text} at (0,23) size 96x22
          text run at (0,23) width 96: "input element "
        RenderImage {INPUT} at (96,22) size 19x17
        RenderText {#text} at (0,0) size 0x0
