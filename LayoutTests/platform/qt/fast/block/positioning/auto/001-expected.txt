layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x576
      RenderBlock {P} at (0,0) size 784x66
        RenderText {#text} at (0,0) size 776x66
          text run at (0,0) width 603: "In this test, you should see three blocks that are centered horizontally within a black box. "
          text run at (603,0) width 159: "They should be stacked"
          text run at (0,22) width 383: "vertically with the green box in between two olive boxes. "
          text run at (383,22) width 393: "The olive boxes and the green box should be 100x100, and"
          text run at (0,44) width 451: "there should be 50 pixels of padding on either side of the box stack."
layer at (8,90) size 240x340
  RenderBlock (positioned) {DIV} at (8,90) size 240x340 [border: (20px solid #000000)]
    RenderBlock {DIV} at (70,70) size 100x0
layer at (78,210) size 100x100
  RenderBlock (positioned) {SPAN} at (70,120) size 100x100 [bgcolor=#008000]
layer at (78,110) size 100x100
  RenderBlock (positioned) {DIV} at (70,20) size 100x100 [bgcolor=#808000]
layer at (78,310) size 100x100
  RenderBlock (positioned) {DIV} at (70,220) size 100x100 [bgcolor=#808000]
