layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x66
        RenderText {#text} at (0,0) size 771x66
          text run at (0,0) width 771: "This test makes sure that HTMLObjectElement.vspace and HTMLObjectElement.hspace are exposed as numbers"
          text run at (0,22) width 769: "instead of strings, per the DOM Level 2 HTML spec: http://www.w3.org/TR/DOM-Level-2-HTML/html.html#ID-"
          text run at (0,44) width 56: "9893177"
      RenderBlock (anonymous) at (0,82) size 784x170
        RenderEmbeddedObject {OBJECT} at (20,10) size 300x150
        RenderText {#text} at (0,0) size 0x0
      RenderBlock {DIV} at (0,252) size 784x22
        RenderText {#text} at (0,0) size 79x22
          text run at (0,0) width 79: "SUCCESS!"
