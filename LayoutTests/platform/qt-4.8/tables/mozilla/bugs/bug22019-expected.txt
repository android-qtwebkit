layer at (0,0) size 784x750
  RenderView at (0,0) size 784x600
layer at (0,0) size 784x750
  RenderBlock {HTML} at (0,0) size 784x750
    RenderBody {BODY} at (8,8) size 768x734
      RenderBlock {PRE} at (0,0) size 768x241
        RenderText {#text} at (0,0) size 440x204
          text run at (0,0) width 303: "These tables below all have the HTML + CSS ..."
          text run at (303,0) width 0: " "
          text run at (0,17) width 0: " "
          text run at (0,34) width 295: "<TABLE><tr>                                          table {"
          text run at (295,34) width 0: " "
          text run at (0,51) width 440: "  <td><TABLE style='margin: 20px; padding: 19px;'>     width: 600px;"
          text run at (440,51) width 0: " "
          text run at (0,68) width 412: "       <tr><td></td></tr>                              border: 1px solid red;"
          text run at (412,68) width 0: " "
          text run at (0,85) width 395: "       </TABLE></td>                                   empty-cells: show;}"
          text run at (395,85) width 0: " "
          text run at (0,102) width 284: "</tr></TABLE>                                        td {"
          text run at (284,102) width 0: " "
          text run at (0,119) width 375: "                                                       border: 1px solid green;}"
          text run at (375,119) width 0: " "
          text run at (0,136) width 392: "                                                     table.inner {margin: 20px;}  "
          text run at (392,136) width 0: " "
          text run at (0,153) width 0: " "
          text run at (0,170) width 388: "... and differ only in the value of 'padding:'. From top table to"
          text run at (388,170) width 0: " "
          text run at (0,187) width 336: "bottom: 0px, 10px, 19px, 20px, 25px. Note that for a "
        RenderInline {U} at (0,0) size 463x35
          RenderInline {I} at (0,0) size 463x36
            RenderText {#text} at (336,187) size 463x36
              text run at (336,187) width 127: "table over-constrained "
              text run at (463,187) width 0: " "
              text run at (0,205) width 112: "within another table"
        RenderText {#text} at (112,205) size 8x17
          text run at (112,205) width 8: ", "
        RenderInline {I} at (0,0) size 143x18
          RenderInline {U} at (0,0) size 143x18
            RenderText {#text} at (120,205) size 143x18
              text run at (120,205) width 143: "when (padding >=margin)"
        RenderText {#text} at (263,205) size 98x17
          text run at (263,205) width 98: ", the margin is "
        RenderInline {I} at (0,0) size 434x36
          RenderInline {U} at (0,0) size 434x36
            RenderText {#text} at (361,205) size 434x36
              text run at (361,205) width 73: "all allocated "
              text run at (434,205) width 0: " "
              text run at (0,223) width 117: "to the right hand side"
        RenderText {#text} at (117,223) size 284x17
          text run at (117,223) width 284: " of the table (and to the left if 'direction: rtl;'."
      RenderTable {TABLE} at (0,254) size 600x32 [border: (1px dotted #0000FF)]
        RenderTableSection {TBODY} at (1,1) size 598x30
          RenderTableRow {TR} at (0,2) size 598x26
            RenderTableCell {TD} at (2,2) size 594x26 [border: (1px solid #008000)] [r=0 c=0 rs=1 cs=1]
              RenderText {#text} at (2,2) size 228x22
                text run at (2,2) width 228: "600px table for width comparison"
      RenderTable {TABLE} at (0,286) size 650x60 [border: (1px solid #FF0000)]
        RenderTableSection {TBODY} at (1,1) size 648x58
          RenderTableRow {TR} at (0,2) size 648x54
            RenderTableCell {TD} at (2,2) size 644x54 [border: (1px solid #008000)] [r=0 c=0 rs=1 cs=1]
              RenderTable {TABLE} at (22,22) size 600x10 [border: (1px solid #FF0000)]
                RenderTableSection {TBODY} at (1,1) size 598x8
                  RenderTableRow {TR} at (0,2) size 598x4
                    RenderTableCell {TD} at (2,2) size 594x4 [border: (1px solid #008000)] [r=0 c=0 rs=1 cs=1]
      RenderTable {TABLE} at (0,346) size 650x80 [border: (1px solid #FF0000)]
        RenderTableSection {TBODY} at (1,1) size 648x78
          RenderTableRow {TR} at (0,2) size 648x74
            RenderTableCell {TD} at (2,2) size 644x74 [border: (1px solid #008000)] [r=0 c=0 rs=1 cs=1]
              RenderTable {TABLE} at (22,22) size 600x30 [border: (1px solid #FF0000)]
                RenderTableSection {TBODY} at (11,11) size 578x8
                  RenderTableRow {TR} at (0,2) size 578x4
                    RenderTableCell {TD} at (2,2) size 574x4 [border: (1px solid #008000)] [r=0 c=0 rs=1 cs=1]
      RenderTable {TABLE} at (0,426) size 650x98 [border: (1px solid #FF0000)]
        RenderTableSection {TBODY} at (1,1) size 648x96
          RenderTableRow {TR} at (0,2) size 648x92
            RenderTableCell {TD} at (2,2) size 644x92 [border: (1px solid #008000)] [r=0 c=0 rs=1 cs=1]
              RenderTable {TABLE} at (22,22) size 600x48 [border: (1px solid #FF0000)]
                RenderTableSection {TBODY} at (20,20) size 560x8
                  RenderTableRow {TR} at (0,2) size 560x4
                    RenderTableCell {TD} at (2,2) size 556x4 [border: (1px solid #008000)] [r=0 c=0 rs=1 cs=1]
      RenderTable {TABLE} at (0,524) size 650x100 [border: (1px solid #FF0000)]
        RenderTableSection {TBODY} at (1,1) size 648x98
          RenderTableRow {TR} at (0,2) size 648x94
            RenderTableCell {TD} at (2,2) size 644x94 [border: (1px solid #008000)] [r=0 c=0 rs=1 cs=1]
              RenderTable {TABLE} at (22,22) size 600x50 [border: (1px solid #FF0000)]
                RenderTableSection {TBODY} at (21,21) size 558x8
                  RenderTableRow {TR} at (0,2) size 558x4
                    RenderTableCell {TD} at (2,2) size 554x4 [border: (1px solid #008000)] [r=0 c=0 rs=1 cs=1]
      RenderTable {TABLE} at (0,624) size 650x110 [border: (1px solid #FF0000)]
        RenderTableSection {TBODY} at (1,1) size 648x108
          RenderTableRow {TR} at (0,2) size 648x104
            RenderTableCell {TD} at (2,2) size 644x104 [border: (1px solid #008000)] [r=0 c=0 rs=1 cs=1]
              RenderTable {TABLE} at (22,22) size 600x60 [border: (1px solid #FF0000)]
                RenderTableSection {TBODY} at (26,26) size 548x8
                  RenderTableRow {TR} at (0,2) size 548x4
                    RenderTableCell {TD} at (2,2) size 544x4 [border: (1px solid #008000)] [r=0 c=0 rs=1 cs=1]
