layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {H1} at (0,0) size 784x42
        RenderText {#text} at (0,0) size 438x42
          text run at (0,0) width 438: "DOMParser/XMLSerializer test"
      RenderBlock {DIV} at (0,63) size 784x44
        RenderText {#text} at (0,0) size 764x44
          text run at (0,0) width 764: "The \"text to parse\" and \"document object serialized\" boxes should show the same text, and it should be an XML"
          text run at (0,22) width 245: "document, not \"@@No result@@\"."
      RenderBlock {DIV} at (0,107) size 784x203 [border: (1px solid #000000)]
        RenderBlock (anonymous) at (1,1) size 782x22
          RenderInline {SPAN} at (0,0) size 83x22 [color=#800000]
            RenderText {#text} at (0,0) size 83x22
              text run at (0,0) width 83: "text to parse"
          RenderText {#text} at (0,0) size 0x0
        RenderBlock {PRE} at (27,36) size 756x153
          RenderText {#text} at (0,0) size 458x153
            text run at (0,0) width 142: "<?xml version=\"1.0\"?>"
            text run at (142,0) width 0: " "
            text run at (0,17) width 344: "<?xml-stylesheet href=\"display.css\" type=\"text/css\"?>"
            text run at (344,17) width 0: " "
            text run at (0,34) width 109: "<!DOCTYPE doc ["
            text run at (109,34) width 0: " "
            text run at (0,51) width 187: "<!ATTLIST d id ID #IMPLIED>"
            text run at (187,51) width 0: " "
            text run at (0,68) width 12: "]>"
            text run at (12,68) width 0: " "
            text run at (0,85) width 38: "<doc>"
            text run at (38,85) width 0: " "
            text run at (0,102) width 458: "  <foo xmlns=\"foobar\">One</foo> <x:bar xmlns:x=\"barfoo\">Two</x:bar>"
            text run at (458,102) width 0: " "
            text run at (0,119) width 155: "  <d id=\"id3\">Three</d>"
            text run at (155,119) width 0: " "
            text run at (0,136) width 46: "</doc>"
            text run at (46,136) width 0: " "
      RenderBlock {DIV} at (0,318) size 784x67 [border: (1px solid #000000)]
        RenderBlock (anonymous) at (1,1) size 782x22
          RenderInline {SPAN} at (0,0) size 112x22 [color=#800000]
            RenderText {#text} at (0,0) size 112x22
              text run at (0,0) width 112: "document object"
          RenderText {#text} at (0,0) size 0x0
        RenderBlock {PRE} at (27,36) size 756x17
          RenderText {#text} at (0,0) size 116x17
            text run at (0,0) width 116: "[object Document]"
      RenderBlock {DIV} at (0,393) size 784x118 [border: (1px solid #000000)]
        RenderBlock (anonymous) at (1,1) size 782x22
          RenderInline {SPAN} at (0,0) size 179x22 [color=#800000]
            RenderText {#text} at (0,0) size 179x22
              text run at (0,0) width 179: "document object serialized"
          RenderText {#text} at (0,0) size 0x0
        RenderBlock {PRE} at (27,36) size 756x68
          RenderText {#text} at (0,0) size 491x68
            text run at (0,0) width 491: "<?xml-stylesheet href=\"display.css\" type=\"text/css\"?><!DOCTYPE doc><doc>"
            text run at (491,0) width 0: " "
            text run at (0,17) width 458: "  <foo xmlns=\"foobar\">One</foo> <x:bar xmlns:x=\"barfoo\">Two</x:bar>"
            text run at (458,17) width 0: " "
            text run at (0,34) width 155: "  <d id=\"id3\">Three</d>"
            text run at (155,34) width 0: " "
            text run at (0,51) width 46: "</doc>"
